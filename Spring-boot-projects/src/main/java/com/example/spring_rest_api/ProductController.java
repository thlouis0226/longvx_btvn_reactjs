package com.example.spring_rest_api;

import java.util.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.http.*;

import org.springframework.web.bind.annotation.*;
@CrossOrigin("http://localhost:3000")
@RestController
public class ProductController {

    @Autowired
    private ProductService service;

    @GetMapping("/products")
    public List<Product> list() {
        return service.listAll();
    }
    @PostMapping("/products")
    public void add(@RequestBody Product product) {
        service.save(product);
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<?> update(@RequestBody Product product, @PathVariable Integer id) {
        try {
            Product existProduct = service.get(id);
            service.save(product);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    // RESTful API method for Delete operation
    @DeleteMapping("/products/{id}")
    public ResponseEntity delete(@PathVariable(value = "id") Integer id) {
        service.delete(id);
        return ResponseEntity.ok().build();
    }
    @GetMapping("/products/{id}")
    public Optional<Product> getone (@PathVariable(value = "id") Integer id){
        return service.getone(id);
    }
}
